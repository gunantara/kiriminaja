import Vue from 'vue'
import App from './App.vue'
import './registerServiceWorker'
import router from './router'
import store from './store'
import VueProgressBar from 'vue-progressbar'
import VueMeta from 'vue-meta'
import VueCompositionAPI from '@vue/composition-api'

Vue.use(VueCompositionAPI)
Vue.use(VueMeta)
Vue.use(VueProgressBar, {
  color: 'rgba(126, 54, 188, 1)',
  failedColor: 'red',
  height: '500px'
})
Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: (h) => h(App)
}).$mount('#app')
