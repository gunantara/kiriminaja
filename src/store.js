import Vue from 'vue'
import Vuex from 'vuex'
import about from './stores/about.js'
Vue.use(Vuex)

const store = new Vuex.Store({
  modules: {
    about
  },
  state: {
    nameapp: 'kiriminAja',
    errors: '',
    isSave: false
  },
  getters: {},
  mutations: {},
  actions: {}
})

export default store
