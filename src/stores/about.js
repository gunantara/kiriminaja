const state = () => ({
  our_leaders: [
    {
      photo: require('../../public/assets/img/team/team-1.png'),
      name: 'Budi Satria Isman',
      role: 'President Commisioner'
    },
    {
      photo: require('../../public/assets/img/team/team-2.png'),
      name: 'Fariz GTJ',
      role: 'Chief Executive Officer & Founder'
    },
    {
      photo: require('../../public/assets/img/team/team-3.png'),
      name: 'Paras M. Nasution',
      role: 'Chief Operation Officer'
    },
    {
      photo: require('../../public/assets/img/team/team-4.png'),
      name: 'Muhammad Irfan',
      role: 'Chief Technology Officer & Founder'
    },
    {
      photo: require('../../public/assets/img/team/team-5.png'),
      name: 'Arief Ardinugroho',
      role: 'Chief Marketing Officer'
    }
  ]
})
const mutations = {}
const actions = {}
export default {
  namespaced: true,
  state,
  actions,
  mutations
}
