import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from './pages/home.vue'
import About from './pages/about.vue'
import Integration from './pages/integration.vue'
Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'home',
    component: Home,
    meta: {
      tag: 'home'
    }
  },
  {
    path: '/about',
    name: 'about',
    component: About,
    meta: {
      tag: 'about'
    }
  },
  {
    path: '/integration',
    name: 'integration',
    component: Integration,
    meta: {
      tag: 'integration'
    }
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  scrollBehavior() {
    return { x: 0, y: 0 }
  },
  linkActiveClass: 'active',
  linkExactActiveClass: 'exact-active',
  routes
})

export default router
